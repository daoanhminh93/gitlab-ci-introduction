variable "region" {
  default = "ap-southeast-1"
}

variable "instance_type" {
  default = "t3.small"
}

variable "project" {
  description = "The project name to use for unique resource naming"
  default     = "cicd-tf"
  type        = string
}

variable "principal_arns" {
  description = "A list of principal arns allowed to assume the IAM role"
  default     = null
  type        = list(string)
}