terraform {
  required_version = ">= 0.14.11"

  required_providers {
    aws    = ">= 4.8.0"
    random = ">= 3.1.0"
  }

  backend "s3" {
    bucket = "cicd-tf-s3-backend"
    key    = "cicd/terraform.tfstate"
    region = "ap-southeast-1"
    # role_arn       = "arn:aws:iam::441162141182:role/Cicd-TfS3BackendRole"
    dynamodb_table = "cicd-tf-s3-backend"
  }
}

# Configure the AWS Provider
provider "aws" {
  region                      = var.region
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
}